# Past weather data sources
## meteo60.fr
### meteo60_data_finder
Download temperature data from meteo60.fr see [README.md](README.md).

### further work
- check if **stationid** is a valid one (i.e. in station_list.csv)
- optionally remove all monthly data after merging

## meteociel.fr
https://www.meteociel.fr/climatologie/obs_villes.php : many data (daily, hourly) from france and worldwide

## ncei.noaa.gov
- https://www.ncei.noaa.gov/access/past-weather/ : very long series (from 194x to today) of US cities and worldwide
    - dataset info at: https://www.ncei.noaa.gov/products/land-based-station/global-historical-climatology-network-daily
    - station list: https://www.ncei.noaa.gov/pub/data/ghcn/daily/ghcnd-stations.txt
    - stationid 07480 = LYON SAINT EXUPERY = bron?
    - stationid 07481 = LYON SAINT EXUPERY = bron?
    - stationid 07156 = PARIS MONTSOURIS
    - same station ids that in meteo60 with country prefix = FRM000 for france, 
    - data directly in csv at: https://www.ncei.noaa.gov/access/past-weather/FRM00007481/data.csv (for 07481 = LYON)
