#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 11:31:15 2022

@author: redondo
"""
############################
# Test energy_data_finder
############################
print('############################')
print('Testing electricity_data_finder')
print('############################')
#1. Import module
print('1. Import module')
from electricity_data_finder import *
#2. get some data from spanish operator
print('2. get_data_es_ree')
get_data_es_ree(yeari=2018,duration=5)
#3. get some data from belgium operator
print('3. get_data_be_elia')
get_data_be_elia()
#4.format spanish data
print('4. format_data_es_ree')
format_data_es_ree()
#5.year summary
print('5. year_summary')
year_summary()
############################
# Test meteo60_data_finder
############################
print('############################')
print('Testing meteo60_data_finder')
print('############################')
#1. Import module
print('1. Import module')
from meteo60_data_finder import *
#2. List stations for dep. '95'
print('2. list_stations')
list_stations('95')
#3.get data for station '07157' (in dep. '95')
print('3. get_data')
get_data(stationid='07157')
#4. merge monthly data in one file
print('4. merge_data')
merge_data(stationid='07157')
