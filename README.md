# Energy Climate Data

Tools to get energy and climate data  from several sources
(energy and climate agencies, grid operators, etc.)
and to get this data usable (homogenise data format).

## Open code versus open data
Code in this repo is licensed under GPL, you can use / distribute / modify it under the terms of this license.

The data that can be obtained with these tools is the property of each data provider and use conditions may differ depending of data owner.
- For this reason, this repo does not contain any data from these data providers.

## Getting the data
Most of data can be obtained manually following instructions in [climatedata.md](climatedata.md) and [energydata.md](energydata.md).
Some data can be obtained automatically (see Meteo60 Data Finder and Electricity Data Finder).
A quite complete set of data can be found at Eco7 shared disk see [data/readme.md](data/readme.md) for more details.

## Requirements
To run this code you will need **python3** and the following modules:
- requests
- requests_html
- pandas
- numpy

You can install these dependencies with **pip**:
~~~
~ pip install -r requirements.txt
~~~

## Getting past weather data from www.ncei.noaa.gov
1. Go to folder containing this code, run python3 and import module
~~~
~ cd path_to_energy_climate_data
~ python3
>>> from ncei_data_finder import *
~~~

2. Get the list of available stations:

(or choose a station in sources/ghcnd-stations.txt)

:warning: First time you run this code you should run the following method:
~~~
format_station_list_file()
~~~
This will convert the original station list file (sources/ghcnd-stations.txt) into a **.csv** (sources/ghcnd-stations.csv).
Then you can use the following method get a list of stations:
~~~
>>> stations = list_stations() # station list in a Panda's dataframe
>>> stations = list_stations('LYON')    # filtered station list for stations containing 'LYON'
>>> stations = list_stations(country_code='FR')    # filtered station list for stations with country code 'FR' (France)
~~~

**stations** is a Pandas' Dataframe with the following data:
- station_id
- latitude
- longitude
- altitude
- location
- country_code

3. Get data from ncei for a stationid:

You can take the **station_id** from the list obtained in the preceding step and put it on the following method:
~~~
>>> get_data(stationid='FR069029001', destdir='./data/0_downloaded/climate/ncei/')
~~~
This will download the data from www.ncei.noaa.gov for the selected **stationid** (**.csv** file in **destdir**).

## Getting past weather data from meteo60.fr
1. Go to folder containing this code, run python3 and import module
~~~
~ cd path_to_energy_climate_data
~ python3
>>> from meteo60_data_finder import *
~~~
2. Get the list of available stations:
~~~
>>> df = list_stations() # station list in a Panda's dataframe
>>> list_stations('69')    # filtered statin list for department 69 (Lyon)
~~~
(or choose a station in sources/meteo60_station_list.csv)

3. Get data monthly from meteo69 for a stationid:
~~~
>>> get_data(stationid='07480',year_ini=2020,year_end=2021,destdir = './data')
~~~
    - all data will be in the **data** (one *.csv file per month)
    - year_ini can be 2014 or later
4. Merge monthly data into one *.csv file
~~~
>>> merge_data(stationid='07480',destdir = './data') 
~~~
- merge all *.csv files in destdir for stationid into 07480_full_data.csv

## Getting electricity mix data
### From REE (Spain)
You can get the data day by day (one file per day).

Example, get 2018 data (366 days):
~~~
>>> get_data_es_ree(yeari=2018,duration=366)
~~~
### From Elia (Belgium)
You can get the data year by year (one file per year).
Example, get 2018 data:
~~~
>>> get_data_be_elia(year=2018)
~~~

