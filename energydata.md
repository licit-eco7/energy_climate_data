# European (wordlwide?) electric mix
## Code (electricity_data_finder)
see [README.md](README.md)

### Getting data
- Spain:
    - get daily data, need to merge into year
- Belgium:
    - get year file but very long (from 2018)

### Formatting data:
- Common file format:
    - one year
    - first column = datetime (utc-0) without summer time
    - second column = total production
    - third column total cons
    - next columns sources
    - units: MW
- TODO by country:
    - AT: rename/reorder columns; summer time
    - BE: rearrange one columns to source columns
    - ES: merge daily, thousans point, summer time, rename/reorder columns
    - FR: encoding, drop 1 row over 2, rename/reorder columns
    - IT: rearrange one columns to source columns, convert to MW, rename/reorder columns, summer time
    - PT: rename/reorder columns, summer time

### Viewing the data

### Possible applications
- analyser l'ensemble des réseaux inscrits à ENTSO-E
    - mix électrique
    - production renouvelable
    - solaire
    - stockage pump-hydro
- dimensionnement optimal des énergies renouvelables à travers l'europe
    - longitude génération
    - longitude consommation
