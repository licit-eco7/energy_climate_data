import os
import re
import requests
from requests.exceptions import ConnectionError
import pandas as pd
import numpy as np

def get_data(stationid='07480',year_ini=2020,year_end=2021,destdir = './data/0_downloaded/climate/meteo60/'):
    if not len(stationid) == 5:
        print('error, please check stationid')
        return
    url_base = 'https://www.meteo60.fr/stations-releves/station-mois-csv'
    
    # see station_list.csv
    #station  = '07480' # 69 Lyon Bron?
    #station  = '07156' # 75 Paris Montsouris
    #year_ini = 2020 #first data in website seems to be march 2014
    #year_end = 2023
    url_list = [url_base + '?stationid=' + stationid +\
                '&annee=' + str(year-2000).zfill(1) +\
                '&mois=' + str(month).zfill(2)
                for year in range(year_ini,year_end) for month in range(1,13)]
    
    #DEBUG
    #url_list = url_list[5:6]
    
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    for url in url_list:
    #    print(url)
        file_out = url.replace(url_base,'')\
                      .replace('?stationid=','')\
                      .replace('&annee=','-')\
                      .replace('&mois=','-') + '.csv'
        file_out = os.path.join(destdir,file_out)
        print(file_out)
        if os.path.exists(file_out):
            print('file exists, skipping')
        try:
            r = requests.get(url)
        #except requests.exceptions.ConnectionError:
        except ConnectionError:
            print(file_out + ' connection error, skipping')
            continue
        if r.status_code==200:
            s = r.text.replace('\t','')
            if len(s)<750:
                print('no data for this month')
                continue
            f = open(file_out,'w+')
            f.write(s)
            f.close()
        #if size of file_out == 0, remove file_out
    print('finished')

def list_stations(dep=''):
    station_file_list = os.path.join('.','sources/meteo60_station_list.csv')
    df = pd.read_csv(station_file_list,sep=';')
    df.columns = ['station_id','location']
    df.station_id = df.station_id.astype(str)
    df.station_id = df.station_id.str.zfill(5)
    
    if dep:
        
        df_filtered = df.loc[df.location.str.startswith(dep)]
    else:
        df_filtered = df
    return df_filtered

def merge_data(stationid='07480',
oridir = './data/0_downloaded/climate/meteo60/',
destdir = './data/1_formatted/climate/meteo60/'):
    file_list = os.listdir(oridir)
    #filter for this stationid:
    file_list = [os.path.join(oridir,f) for f in file_list if re.match('^' + stationid + '-[0-9][0-9]-[0-9][0-9].csv',f)]
    df_list = list()
    for f in file_list:
        words = f.split('-')
        df = pd.read_csv(f,sep=';')
        df.dropna(axis=1,inplace=True)
        df.columns = ['Jour','Tmin','Tmax','Tavg','Prec','Sun']
        df['stationid'] = stationid
        df['year'] = words[1]
        df['month'] = words[2].replace('.csv','')
        df['day'] = df.Jour.str[-2:]
        df['date'] = df.year.astype(str) + '-' + df.month.astype(str) + '-' + df.day.astype(str)
        #df['date'] = pd.to_datetime(df.date,format='%y-%m-%d')
        df = df.reindex(['date','Tmin','Tmax','Tavg'],axis=1)
        
        #clean '-' as nan (no data)
        df.loc[df.Tmin.astype(str) == '-'] = np.nan
        df.loc[df.Tmax.astype(str) == '-'] = np.nan
        df.loc[df.Tavg.astype(str) == '-'] = np.nan
        #cast to lfoat
        df.Tmin = df.Tmin.astype(float)
        df.Tmax = df.Tmax.astype(float)
        df.Tavg = df.Tavg.astype(float)
        
        df_list.append(df)
    if df_list:
        if not os.path.exists(destdir):
            os.mkdir(destdir)
        df_all = pd.concat(df_list,ignore_index=True)
        df_all.sort_values(by='date',ignore_index=True,inplace=True)
        file_out = os.path.join(destdir,stationid + '_full_data.csv')
        df_all.to_csv(file_out,sep=';',index=False)
        return df_all
    else:
        print('no found data, please try another stationid')
