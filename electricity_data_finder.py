#web: 32 MB for 2 years (730 days), 45kB/day
#csv: 9.5 MB for 2 years (730 days), 13KB / day

import requests
from datetime import datetime, timedelta
import os, re
from requests.exceptions import ConnectionError
from requests_html import HTMLSession
from requests_html import AsyncHTMLSession
import pandas as pd

#async def get_data(yeari=2021,monthi=1,dayi=1,duration=31):
def get_data_es_ree(yeari=2018,monthi=1,dayi=1,duration=31,
destdir = './data/0_downloaded/elec_mix_time_series/es_ree'):
    session = HTMLSession()
    #session = AsyncHTMLSession()
    
    url_head = 'https://demanda.ree.es/visiona/peninsula/demanda/tablas/'
    url_tail = '/2'
    
    start = datetime(yeari, monthi, dayi)
    tdeltas = [timedelta(days=d) for d in range(duration)]
    days = [start+td for td in tdeltas]
    
    
    #days = days[19:20]
    if not os.path.exists(destdir):
        try:
            os.mkdir(destdir)
        except:
            print('unable to write destdir')
            return
    for day in days:
        url_day = day.date().strftime('%Y-%m-%d')
        file_out = os.path.join(destdir,url_day+'.csv')
        if os.path.exists(file_out):
            if os.path.getsize(file_out)>1000:
                print(file_out + ' already exists, skipping')
                continue
        print(url_day)
        url = url_head+url_day+url_tail
        #r = requests.get(url)
        try:
            r = session.get(url)
            #r = await session.get(url)
        #except requests.exceptions.ConnectionError:
        except ConnectionError:
            print(file_out + ' connection error, skipping')
            continue
        if r.status_code==200:
            r.html.render(sleep=1)
            #await r.html.arender(sleep=1)
            f = open(file_out,'w+')
            tbody = r.html.find('tbody')
            ths = r.html.find('th')
            skip_th = True
            for th in ths:
                s = th.full_text
                if s=='Hora':
                    skip_th = False
                elif s.startswith('CO2'):
                    skip_th = True
                if not skip_th:
                    f.writelines(s + '\t')
            tds = r.html.find('td')
            #f.writelines('\n')
            for td in tds:
                s = td.full_text
                if s.find(':')<0:
                    f.writelines('\t' + s)
                else:
                    f.writelines('\n' + s)
            f.close()
        #if size of file_out == 0, remove file_out
    print('finished')

def get_data_be_elia(year=2018,destdir='./data/0_downloaded/elec_mix_time_series/be_elia'):
    url = 'https://opendata.elia.be/explore/dataset/ods033/download/?format=csv&refine.datetime='\
          +str(year)+'&timezone=Europe/Brussels&lang=fr&use_labels_for_header=true&csv_separator=%3B'
    file_out = os.path.join(destdir,'elia_'+str(year)+'_ods033.csv')
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    if os.path.exists(file_out):
        print(file_out + ' already exists, skipping')
        return
    print('getting: ' + url)
    r = requests.get(url)
    print('write file: ' + file_out)
    if r.status_code==200:
        f = open(file_out,'w+')
        s = r.content.decode("utf-8")
        f.write(s)
        f.close()
    print('finished')

def format_data_fr_rte(year=2018,
oridir = './data/0_downloaded/elec_mix_time_series/fr_rte/',
destdir = './data/1_formatted/elec_mix_time_series/fr_rte/'):
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    filelist = os.listdir(oridir)
    filelist = [f for f in filelist if re.match('^eCO2mix_RTE_Annuel-Definitif_' + str(year) +'\.xls$',f)]
    
    if filelist:
        pathname = os.path.join(oridir,filelist[0])
    else:
        print('file not found at fr_rte')
        return
    #read xls files as csv
    df = pd.read_csv(pathname,sep='\t',encoding='latin_1')
    #problem with headings
    col_list = list(df.columns)
    col_list.append('unnamed')
    col_list.pop(0)
    df.columns = col_list
    
    #remove last column (all nans)
    df = df.dropna(axis=1,how='all')
    #remove quarterly nans (consumption forecasts)
    df = df.dropna(axis=0,how='any')
    
    #remove uninteresting columns
    df.drop(labels=['Nature','Prévision J-1','Prévision J','Taux de Co2'],
            axis=1,inplace=True)
    #DEBUG
#    return df
    df_out = standard_df()
    df_out.Date = df.Date.astype(str) + ' ' + df.Heures.astype(str)
    #TODO now we need to correct the time change
    df_out.Date = pd.to_datetime(df_out.Date)
    
    df_out.Consumption = df.Consommation
    df_out.Exchanges = df['Ech. physiques']
    df_out.Nuclear = df.Nucléaire
    df_out.Gas = df.Gaz
    df_out.Fuel = df.Fioul
    df_out.Coal = df.Charbon
    df_out.Wind = df.Eolien
    df_out.Solar = df.Solaire
    df_out.Hydro = df.Hydraulique
    df_out.Pumped = df.Pompage
    df_out.Bio_energy = df.Bioénergies
    
    #df_out = df_out.fillna(0)
    df_out.Total_fossile = df_out.Gas+df_out.Coal+df_out.Fuel
    df_out.Total_renewables = df_out.Wind+df_out.Hydro+df_out.Solar+df_out.Bio_energy
    df_out.Production = df_out.Nuclear + df_out.Total_fossile + df_out.Total_renewables
    
    df_out.reset_index(drop=True,inplace=True)
    file_out = os.path.join(destdir,str(year) + '.csv')
    df_out.to_csv(file_out,sep='\t',index=False)
    return df_out

def format_data_es_ree(year=2018,
oridir = './data/0_downloaded/elec_mix_time_series/es_ree',
destdir = './data/1_formatted/elec_mix_time_series/es_ree'):
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    
    filelist = os.listdir(oridir)
    filelist = [f for f in filelist if re.match('^' + str(year) +'-.*\.csv$',f)]
    #filelist = filelist[0:5]#DEBUG
    
    df_list = [pd.read_csv(os.path.join(oridir,f),thousands='.',sep='\t') for f in filelist]
    df_list = [df.dropna(axis=1) for df in df_list]
    
    df = pd.concat(df_list,ignore_index=True)
    #remove duplicates (each day's file starts before and finishes after midnigth)
    df = df.drop_duplicates()
    #order by datetime
    df.sort_values(by='Hora',inplace=True,ignore_index=True)
    #remove some data in hours preceding postponing this year
    df = df.loc[df.Hora.str.contains('^' + str(year) +'-')]
    
    # create standard dataframe:
    df_out = standard_df()
    
    df_out.Date = df.Hora
        #In winter time change day:
    # '02:MM' is marked as '2A:MM' before the change
    # '02:MM' is marked as '2B:MM' after the change
    df_out.Date = df_out['Date'].astype(str).str.replace(' 2A:',' 02:')
    df_out.Date = df_out['Date'].astype(str).str.replace(' 2B:',' 02:')
    #TODO now we need to correct the time change
    df_out.Date = pd.to_datetime(df_out.Date)
    #DEBUG
    #return df
    # TODO
    #df_out.Consumption = update get_data_es_ree
    #df_out.Pumped = not in these files
    #Columns common to all years
    df_out.Exchanges = df['Intercambios int'] + df['Enlace balear']
    df_out.Nuclear = df.Nuclear
    df_out.Gas = df['Ciclo combinado']
    df_out.Coal = df['Carbón']
    df_out.Wind = df['Eólica']
    df_out.Hydro = df['Hidráulica']
    #Always zero or in 2019 missing column
    #df_out.Fuel = df['Fuel/gas']
    
    # TODO:
    # Before 25/07/2013 Spanish solar production was 'Resto reg.esp.' column.
    # Need to separate Bio_energy and solar (at the moment all is in bio_energy)
    
    if year<2015:
        df_out.Solar = df['Solar']
        df_out.Bio_energy = df['Cogeneración y residuos'] + df['Resto reg.esp.']
    elif year==2015:
        df_out.Solar = df['Solar fotovoltaica'] + df['Solar térmica']
        df_out.Bio_energy = df['Térmica renovable'] +\
                df['Cogeneración y residuos'] +\
                df['Resto reg.esp.']
    else:
        df_out.Solar = df['Solar fotovoltaica'] + df['Solar térmica']
        df_out.Bio_energy = df['Térmica renovable'] + df['Cogeneración y residuos'] 
        
    df_out = df_out.fillna(0)
    
    df_out.Total_fossile = df_out.Gas+df_out.Coal+df_out.Fuel
    df_out.Total_renewables = df_out.Wind+df_out.Hydro+df_out.Solar+df_out.Bio_energy
    df_out.Production = df_out.Nuclear + df_out.Total_fossile + df_out.Total_renewables
    
    
    file_out = os.path.join(destdir,str(year) + '.csv')
    df_out.to_csv(file_out,sep='\t',index=False)
    
    return df_out

def format_data_de_smard(year=2018,
oridir = './data/0_downloaded/elec_mix_time_series/de_smard',
destdir = './data/1_formatted/elec_mix_time_series/de_smard'):
    #TODO missing consumption and exchanges
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    filelist = os.listdir(oridir)
    filelist = [f for f in filelist if re.match('^Actual_generation_' + str(year) + '01010000',f)]
    
    if filelist:
        pathname = os.path.join(oridir,filelist[0])
    else:
        print('file not found at de_smard')
        return
    #read xls files as csv
    df = pd.read_csv(pathname,sep=';',thousands=',')
    
        # create standard dataframe:
    df_out = standard_df()
    
    df_out.Date = df['Date'].astype(str) + ' ' + df['Time of day'].astype(str)
    df_out.Date =  pd.to_datetime(df_out.Date)
    #'Consumption'
    #'Exchanges'
    df_out.Nuclear = df['Nuclear[MWh]']*4
    df_out.Gas = df['Fossil gas[MWh]']*4
    #'Fuel'
    df_out.Coal = df['Lignite[MWh]'] + df['Hard coal[MWh]']*4
    df_out.Wind = df['Wind offshore[MWh]'] + df['Wind onshore[MWh]']*4
    df_out.Solar = df['Photovoltaics[MWh]']*4
    df_out.Hydro = df['Hydropower[MWh]']*4
    #Pumped should be negative (power consumption to storage)
    df_out.Pumped = -df['Hydro pumped storage[MWh]']*4
    df_out.Bio_energy = df['Biomass[MWh]']*4
    
    #Unkown field in df:'Other renewable[MWh]','Other conventional[MWh]'
    
    df_out = df_out.fillna(0)
    
    df_out.Total_fossile = df_out.Gas+df_out.Coal+df_out.Fuel
    df_out.Total_renewables = df_out.Wind+df_out.Hydro+df_out.Solar+df_out.Bio_energy
    df_out.Production = df_out.Nuclear + df_out.Total_fossile + df_out.Total_renewables
    return df_out

def year_summary(country='es',year='2018'):
    
    destdir = './data/1_formatted/elec_mix_time_series/'
    countrydir = os.listdir(destdir)
    countrydir = [c for c in countrydir if c.startswith(country)]
    if not countrydir:
        print('country not found')
        return
    
    if len(countrydir)>1:
        print('more than one folder for this country code, specify grid operator')
        return
    
    countrydir = countrydir[0]
    
    filelist = os.listdir(os.path.join(destdir,countrydir))
    filelist = [f for f in filelist if f == str(year) + '.csv']
    pathname = os.path.join(destdir,countrydir,filelist[0])
    
    df = pd.read_csv(pathname,sep='\t')
    E_wind_TWh = df.Wind.mean() * 365 * 24 / 1000000
    E_solar_pv_TWh = df['Solar_PV'].mean() * 365 * 24 / 1000000
    E_solar_th_TWh = df['Solar_Therm'].mean() * 365 * 24 / 1000000
    E_nuclear_TWh = df.Nuclear.mean() * 365 * 24 / 1000000
    
    print('Wind TWh: ' + str(E_wind_TWh))
    print('Solar PV TWh: ' + str(E_solar_pv_TWh))
    print('Nuclear TWh: ' + str(E_nuclear_TWh))

def standard_df():
    col_list = [
'Date','Consumption','Production','Exchanges','Nuclear','Gas','Fuel','Coal','Wind','Solar','Hydro','Pumped','Bio_energy',
'Total_fossile','Total_renewables']
    return pd.DataFrame(columns=col_list)