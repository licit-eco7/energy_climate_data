#!/usr/bin/env/ python3
# -*- coding: utf-8 -*-

"""
Created on Tue Apr  9 8:00:00 2019

@author: eduvieilbourg
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
from matplotlib.dates import DateFormatter

def plot_energy_data(df):
    #TODO: why french data is plotted with another format (french month names)??
    fig, ax = plt.subplots(2,2)
    df.plot(x='Date',y=['Consumption','Production','Exchanges'],ax=ax[0][0])
    df.plot(x='Date',y=['Total_fossile','Nuclear','Total_renewables'],ax=ax[0][1])
    df.plot(x='Date',y=['Gas','Fuel','Coal'],ax=ax[1][0])
    df.plot(x='Date',y=['Wind','Solar','Hydro','Pumped','Bio_energy'],ax=ax[1][1])
    
#    Not working with fr_rte data
#    date_form = DateFormatter("%Y-%m-%d %H:%M")
#    ax[0][0].xaxis.set_major_formatter(date_form)
#    ax[0][1].xaxis.set_major_formatter(date_form)
#    ax[1][0].xaxis.set_major_formatter(date_form)
#    ax[1][1].xaxis.set_major_formatter(date_form)
    
def plot_energy_data_interactive(df):
    #TODO still not working
    
    #create figure and axes
    fig, ax = plt.subplots()
    
    # plot in created axes
    df.plot(x='Date',ax=ax)
    
    plt.subplots_adjust(left=0.25)
    #format x labels with HH:MM
    date_form = DateFormatter("%Y-%m-%d %H:%M")
    ax.xaxis.set_major_formatter(date_form)
    
    l_list = ax.get_lines()
    l_list = list(l_list)
    rax = plt.axes([0.05, 0.1, 0.15, 0.85])
    labels = [str(line.get_label()) for line in l_list]
    visibility = [line.get_visible() for line in l_list]
    check = CheckButtons(rax, labels, visibility)
    def func(label):
        index = labels.index(label)
        lines[index].set_visible(not lines[index].get_visible())
        plt.draw()
    
    check.on_clicked(func)
    
    plt.show()
    
def plot_data(df):
    #Example function with interactive plot
    #From: https://matplotlib.org/stable/gallery/widgets/check_buttons.html
    #User can select/deslect line to visualize
    t = np.arange(0.0, 2.0, 0.01)
    s0 = np.sin(2*np.pi*t)
    s1 = np.sin(4*np.pi*t)
    s2 = np.sin(6*np.pi*t)
    
    fig, ax = plt.subplots()
    l0, = ax.plot(t, s0, visible=False, lw=2, color='k', label='2 Hz')
    l1, = ax.plot(t, s1, lw=2, color='r', label='4 Hz')
    l2, = ax.plot(t, s2, lw=2, color='g', label='6 Hz')
    plt.subplots_adjust(left=0.2)
    
    lines = [l0, l1, l2]
    
    # Make checkbuttons with all plotted lines with correct visibility
    rax = plt.axes([0.05, 0.4, 0.1, 0.15])
    labels = [str(line.get_label()) for line in lines]
    visibility = [line.get_visible() for line in lines]
    check = CheckButtons(rax, labels, visibility)
    
    
    def func(label):
        index = labels.index(label)
        lines[index].set_visible(not lines[index].get_visible())
        plt.draw()
    
    check.on_clicked(func)
    
    plt.show()
    
