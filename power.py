'''
*Version: 2.0 Published: 2021/03/09* Source: [NASA POWER](https://power.larc.nasa.gov/)
POWER API Multi-Point Download
This is an overview of the process to request data from multiple data points from the POWER API.
'''

import os, json, requests

locations = [(32.929, -95.770), (5, 10)]

output = r""
base_url = r"https://power.larc.nasa.gov/api/temporal/daily/point?parameters=T2M,T2MDEW,T2MWET,TS,T2M_RANGE,T2M_MAX,T2M_MIN&community=RE&longitude={longitude}&latitude={latitude}&start=20150101&end=20150305&format=CSV"

for latitude, longitude in locations:
    api_request_url = base_url.format(longitude=longitude, latitude=latitude)

    
    try:
        r = requests.get(url=api_request_url, verify=True, timeout=30.00)
    #except requests.exceptions.ConnectionError:
    except ConnectionError:
        print(fileout + ' connection error, skipping')

    if r.status_code==200:
        f = open("toto.csv",'w+')
        s = r.content.decode("utf-8")
        f.write(s)
        f.close()
