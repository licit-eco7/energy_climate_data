import os
import re
import requests
from requests.exceptions import ConnectionError
import pandas as pd
import numpy as np

def get_data(stationid='FR069029001',destdir = './data/0_downloaded/climate/ncei/'):
    if not len(stationid) == 11:
        print('error, please check stationid')
        return
    url_base = 'https://www.ncei.noaa.gov/access/past-weather/'
    url_tail = '/data.csv'
    
    url = url_base + stationid + url_tail
    
    if not os.path.exists(destdir):
        os.mkdir(destdir)
        
    fileout = stationid + '.csv'
    pathname = os.path.join(destdir,fileout)
    if os.path.exists(pathname):
        print(fileout + ' file already exists, skipping')
    try:
        r = requests.get(url)
    #except requests.exceptions.ConnectionError:
    except ConnectionError:
        print(fileout + ' connection error, skipping')
        return
    if r.status_code==200:
        f = open(pathname,'w+')
        s = r.content.decode("utf-8")
        f.write(s)
        f.close()
        #if size of file_out == 0, remove file_out
    print('finished')

def format_data(stationid='FR069029001',year_ini=2018,year_fin=2021,
oridir = './data/0_downloaded/climate/ncei/',
destdir = './data/1_formatted/climate/ncei/'):
    #1. load file in oridir for given station_id
    pathname = os.path.join(oridir,stationid+'.csv')
    if not os.path.exists(pathname):
        print(pathname + ' not found')
        return
    df = pd.read_csv(pathname,skiprows=1)
    #2. convert Farenheit into Celsius
    #3. cut into years (one file per year)
    df_years = [df.loc[df.Date.str.startswith(str(year))] for year in range(year_ini,year_fin+1)]
    #remove empty results
    df_years = [dfy for dfy in df_years if len(dfy)]
    #4. #TODO:save each year file in destdir
    #5. return list of dataframes with selected years
    return df_years

def list_stations(location_filter='',country_code='',coordinates=(),
                  station_file_list = './sources/ghcnd-stations.csv'):
    
    if not os.path.exists(station_file_list):
        print('Station list file not found: ' + station_file_list)
        return
    stations = pd.read_csv(station_file_list,header=None,sep=',')
    stations.columns = ['station_id','latitude', 'longitude', 'altitude', 'location']
    stations['country_code'] = stations.station_id.str[0:2]
    if location_filter:
        stations_filtered = stations.loc[stations.location.str.contains(location_filter)]
    else:
        stations_filtered = stations
    if country_code:
        stations_filtered = stations_filtered.loc[stations_filtered.country_code==country_code]
    if coordinates:
        stations_filtered['D'] = sphere_distance(coordinates[0],coordinates[1],stations_filtered.latitude,stations_filtered.longitude)
        stations_filtered.sort_values(by='D',inplace=True)
    return stations_filtered

def format_station_list_file(filename='./sources/ghcnd-stations.txt'):
    #little method to convert fixed column file list from ghcnd into
    #a csv file
    
    #read the file
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    #pslit every line into station id (11 alphanumeric characteers and the rest of the line:
    words = [re.split('(?<=^[^\s]{11,11})\s+',l,1) for l in lines]
    station_ids = [w[0] for w in words]
    other_info = [w[1].replace(',',' ') for w in words]
    other_info = [re.split('(?<=[0-9])\s+',w,3) for w in other_info]
    lines2 = [','.join(w) for w in other_info]
    
    #put every station id with rest of info in lines 3
    lines3 = [s+','+l for s,l in zip(station_ids,lines2)]
    
    
    fileout = re.sub('.txt$','.csv',filename)
    f = open(fileout, 'w+')
    f.writelines(lines3)
    f.close()

def sphere_distance(latitude0,longitude0, latitude1,longitude1):
    #distance fllowing the formula given in 
    #https://www.calculator.net/distance-calculator.html
    R = 6371
    lat0_rad = np.deg2rad(latitude0)
    lon0_rad = np.deg2rad(longitude0)
    lat1_rad = np.deg2rad(latitude1)
    lon1_rad = np.deg2rad(longitude1)
    
    a = (lat1_rad - lat0_rad)/2
    b = lat0_rad
    c = lat1_rad
    d = (lon1_rad - lon0_rad)/2
    
    distance = 2*R*np.arcsin(
                    np.sqrt(
                     np.sin(a)**2+np.cos(b)*np.cos(c)*(np.sin(d)**2)
                     )
                    )
    
    return distance
    